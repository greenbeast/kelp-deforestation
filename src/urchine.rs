use rand::Rng;

/// Would love to make this into a monte carlo

/// Struct used to store some variables
struct Vals {
     height: Vec<i32>, // Meters (100-260 feet)
     area: i32, // 6.2 Million Square Meters
     urchin_num: i32, // Number of urchins
     urchin_intake: i32, // The amount they eat
}

/// This function calculates the destruction of the kelp forrest
pub fn kelp() {
   let num = rand::thread_rng().gen_range(0..6); // Random number from 0-5
   let vars = Vals{height: [100,130,160,190,220,260].to_vec(), area: 6200000, urchin_num: 100, urchin_intake: 12};
   let rand_height: i32 = vars.height[num];
   // height times the area to get the volume, need to figure out float times int
   let mut forrest_volume = rand_height * vars.area;
   let urchins = vars.urchin_num; // urchin repopulation
   let mut days: i32 = 0;
   // Amount eaten in a day
   let intake_num = rand::thread_rng().gen_range(3..7); // Random number from 3-6
   let intake: i32 = vars.urchin_intake * intake_num * urchins; // random amount of food eaten each day between 12-24 inches
   println!("Randomized height {}, and forrest volume {}", rand_height, forrest_volume);

   while forrest_volume > 0 {
   	 forrest_volume = forrest_volume - intake;
	 days += 1;
   }
   println!("{} days to get forest volume to {}", days, forrest_volume);
}